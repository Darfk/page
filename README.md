## Synopsis

A tiny html page renderer 

## Code Example

See other repo: https://bitbucket.org/Darfk/mux

## Motivation

I needed a framework for displaying HTML pages in golang.

## Installation

`go get github.com/darfk/mux`

## Tests

*No tests*

## License

MIT
